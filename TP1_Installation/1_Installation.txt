************************************************************************
Installation d'Apache Kafka
************************************************************************

Prérequis : Oracle Java JDK 1.8 ou plus récent
http://www.oracle.com/technetwork/java/javase/downloads/index.html

========================================================================
Gestion des différentes versions de Java sous Ubuntu
========================================================================
$ update-alternatives --display java
$ update-alternatives --config java
$ java -version

========================================================================
1. Téléchargement et installation de Kafka
========================================================================

Télécharger la version compilée 0.11.0.1 d'Apache Kafka pour Scala 2.11
à l'adresse https://kafka.apache.org/downloads

Extraire le contenu de l'archive dans un dossier de votre choix (ici
ce sera /d/bin/)

Aller dans le dossier /d/bin/kafka_2.11-0.11.0.1/


========================================================================
2. Lancement du serveur Kafka
========================================================================

Pour GNU-Linux et MacOS utiliser les programmes du dossier ./bin
avec l'extension .sh

Pour MS Windows, un dossier ./bin/windows contient leur équivallent avec
l'extensiton .bat


Lancement de Zookeeper
----------------------
Ouvrir une console et taper la commande suivante

>$ .\bin\windows\zookeeper-server-start.bat config\zookeeper.properties

Lancement du serveur Kafka
--------------------------
Ouvrir une console et taper la commande suivante

>$ bin\windows\kafka-server-start.bat config\server.properties


========================================================================
3. Création d'un Topic
========================================================================

Ouvrir une autre console dans le dossier d'installation de Kafka

Lancer la création d'un Topic en tapant la commande

>$  bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
Created topic "test".


========================================================================
4. Visualiser les Topics
========================================================================

Pour visualiser les Topics créés dans Kafka taper la commande

>$ bin\windows\kafka-topics.bat --list --zookeeper localhost:2181 
test                                                             


========================================================================
5. Envoie et réception de message dans Kafka
========================================================================

Lancer un Consumer pour lire les messages que nous allons envoyer avec
un Producer

>$  bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic test --from-beginning


Ouvrir une nouvelle console pour lancer le Producer

>$ bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic test
>                                                                                 

Le prompt étant affiché, vous pouvez taper des message et les voir
s'afficher dans la console du Consumer.

========================================================================
6. Relecture des messages d'une file
========================================================================
Arrêter le Consumer (ctrl+c) et le relancer. Que constatez-vous ?

========================================================================
7. Ajout de messages
========================================================================
Tapez à nouveau des messages dans le Producer. Que constatez-vous ?

Done!!

========================================================================
7. Création d'un broker Kafka avec plusieurs partions et des facteurs
   de réplication/duplication différents
========================================================================

$ bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 3 --topic multipartitions1

$ bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 3 --partitions 3 --topic multipartitions2

$ bin\windows\kafka-topics.bat --describe --zookeeper localhost:2181

