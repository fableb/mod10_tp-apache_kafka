package MultiBroker;

// Exemple 1
// ---------
//PS D:\bin\kafka_2.11-0.11.0.1> bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 3 --topic multibroker1
//Created topic "multibroker1".
//PS D:\bin\kafka_2.11-0.11.0.1> bin\windows\kafka-topics.bat --describe --zookeeper localhost:2181 --topic multibroker1
//Topic:multibroker1      PartitionCount:3        ReplicationFactor:1     Configs:
//Topic: multibroker1     Partition: 0    Leader: 2       Replicas: 2     Isr: 2
//Topic: multibroker1     Partition: 1    Leader: 0       Replicas: 0     Isr: 0
//Topic: multibroker1     Partition: 2    Leader: 1       Replicas: 1     Isr: 1

// Exemple 2
// ---------
//PS D:\bin\kafka_2.11-0.11.0.1> bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 3 --partitions 3 --topic multibroker2
//Created topic "multibroker2".
//PS D:\bin\kafka_2.11-0.11.0.1> bin\windows\kafka-topics.bat --describe --zookeeper localhost:2181 --topic multibroker2
//Topic:multibroker2      PartitionCount:3        ReplicationFactor:3     Configs:
//Topic: multibroker2     Partition: 0    Leader: 1       Replicas: 1,0,2 Isr: 1,0,2
//Topic: multibroker2     Partition: 1    Leader: 2       Replicas: 2,1,0 Isr: 2,1,0
//Topic: multibroker2     Partition: 2    Leader: 0       Replicas: 0,2,1 Isr: 0,2,1

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class MultiBrokerProducer {
    private static final Properties properties = new Properties();
    private static Producer<Integer, String> producer;

    public MultiBrokerProducer() {
        properties.put("bootstrap.servers", "localhost:9092"); // adresse du broker
        properties.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("partitioner.class", "MultiBroker.CustomPartitioner"); // partitioneur 'maison"
        properties.put("acks", "all"); // le broker doit envoyer un ack quand le message a été complètement répliqué

        this.producer = new KafkaProducer<Integer, String>(properties); // paramétrage du producer
    }

    public static void main(String[] args) {
        MultiBrokerProducer multiBrokerProducer = new MultiBrokerProducer();
        String topic = "multibroker2";

        for (int i = 0; i < 10; i++) {
            String messageStr = "Message #" + i + " pour le topic '" + topic + "'";
            ProducerRecord<Integer, String> message = new ProducerRecord<Integer, String>(
                    topic,
                    i,
                    messageStr);
            multiBrokerProducer.producer.send(message);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        multiBrokerProducer.producer.close();
    }
}
