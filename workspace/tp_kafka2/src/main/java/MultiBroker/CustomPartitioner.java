package MultiBroker;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

public class CustomPartitioner implements Partitioner {

    public int partitionAlgorithm(int key, int numPartitions){
        return key % numPartitions;
    }

    public int partition(String topic,
                         Object key,
                         byte[] keyBytes,
                         Object value,
                         byte[] valueBytes,
                         Cluster cluster) {
        int partition = 0;
        int iKey = (Integer) key;
        int numPartitions = cluster.availablePartitionsForTopic(topic).size();

        if(iKey >= 0) {
            partition = partitionAlgorithm(iKey, numPartitions);
        }
        return partition;
    }

    public void close() {

    }

    public void configure(Map<String, ?> map) {

    }
}
