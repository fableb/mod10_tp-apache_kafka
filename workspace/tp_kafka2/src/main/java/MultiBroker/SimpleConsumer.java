package MultiBroker;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;

public class SimpleConsumer {
    private static final Properties properties = new Properties();
    private static Consumer<Integer, String> consumer;

    public SimpleConsumer() {
        properties.put("bootstrap.servers", "localhost:9092"); // adresse du broker
        properties.put("group.id", "group_test");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.IntegerDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        this.consumer = new KafkaConsumer<Integer, String>(properties);
        //this.consumer.subscribe(Arrays.asList("topic1")); // liste des topics pour lesquels souscrire
        //this.consumer.subscribe(Arrays.asList("topic_1","topic_2","topic_3")); // liste des topics pour lesquels souscrire
        this.consumer.subscribe(Arrays.asList("words-count")); // liste des topics pour lesquels souscrire
    }

    public static void main(String[] args) {
        SimpleConsumer simpleConsumer = new SimpleConsumer();
        while(true){
            ConsumerRecords<Integer, String> records = simpleConsumer.consumer.poll(1);
            for(ConsumerRecord<Integer, String> record: records) {
                System.out.println("partition = " + record.partition()
                        + " offset = " + record.offset()
                        + " key = " + record.key()
                        + " value = " + record.value());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}