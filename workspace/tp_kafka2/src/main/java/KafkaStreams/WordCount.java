package KafkaStreams;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.kstream.KTable;

import java.util.Arrays;
import java.util.Properties;

public class WordCount {
    public static void main(String[] args) throws InterruptedException {
        // Configuration
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "workdcount");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        KStreamBuilder builder = new KStreamBuilder();

        // 1. Récupération du flux de Kafka (input)
        KStream<String, String> inputs = builder.stream("kafka-streams");

        // Word count
        KTable<String, Long> wordsCount = inputs
                .mapValues(value -> value.toLowerCase()) // 2. Transformer les caractères en minuscules
                .flatMapValues(value -> Arrays.asList(value.split(" "))) // 3. Tokenisation des chaines
                .selectKey((key, value) -> value) // 4. Séléction des clés
                .groupByKey() // 5. Regroupement par clé
                .count("Counts"); // 6. Comptage des mots et stockage avec un nom logique 'Counts'

        // 7. Ecriture dans un topic Kafka (output)
        wordsCount.to(Serdes.String(), Serdes.Long(), "words-count");

        // Création du flux Kafka
        KafkaStreams streams = new KafkaStreams(builder, config);
        streams.cleanUp();
        streams.start(); // demarrage de l'application

        // Pour fermer le programme sinon il tourne sans fin
        Thread.sleep(5000L);
        streams.close();
    }
}
