package SparkKafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.*;
import org.apache.kafka.common.serialization.StringDeserializer;

import scala.Tuple2;

import java.util.*;

public class SparkKafka {
    public static void main(String[] args) throws InterruptedException {
        // Création du contexte avec un intervalle de traitement par lot ('batch') de 2 secondes
        SparkConf sparkConf = new SparkConf()
                .setMaster("local")
                .setAppName("JavaDirectKafkaWordCount");
        JavaStreamingContext streamingContext = new JavaStreamingContext(
                sparkConf,
                Durations.seconds(2)); // Regroupement des messages Kafka sur une période de 2 secondes

        Collection<String> topics = Arrays.asList("multibroker2");

        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", "localhost:9092");
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "use_a_separate_group_id_for_each_stream");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);

        // Création de la connexion au stream Kafka avec un 'Discretized Stream Spark'
        final JavaInputDStream<ConsumerRecord<String, String>> stream =
                KafkaUtils.createDirectStream(
                        streamingContext,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.Subscribe(topics, kafkaParams));

        // Récupération et affichage des enregistrements du flux Kafka sous la forme de paires (key, value)
        JavaPairDStream<String, String> stringStringJavaPairDStream = stream.mapToPair(
                new PairFunction<ConsumerRecord<String, String>, String, String>() {
                    @Override
                    public Tuple2<String, String> call(
                            ConsumerRecord<String, String> record) {
                        return new Tuple2<>(record.key(), record.value());
                    }
                });
        stringStringJavaPairDStream.print();

        streamingContext.start(); // Début des calculs sur les flux ('streams')
        streamingContext.awaitTermination();
    }
}
