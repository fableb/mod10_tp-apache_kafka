package SingleBroker;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class SimpleProducer {
    private static final Properties properties = new Properties();
    public static Producer<String, String> producer;

    // constructeur
    public SimpleProducer() {
        properties.put("bootstrap.servers", "localhost:9092"); // adresse du broker
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("acks", "all"); // le broker doit envoyer un ack quand le message a été complètement répliqué

        this.producer = new KafkaProducer<String, String>(properties); // paramétrage du producer
    }

    public static void main(String[] args) {
        SimpleProducer simpleProducer = new SimpleProducer();
        String topic = "test";

        for (int i = 0; i < 100; i++) {
            String messageStr = "Message #" + i + " pour le topic '" + topic + "'";
            ProducerRecord<String, String> message = new ProducerRecord<String, String>(topic,
                    Integer.toString(i), messageStr);
            simpleProducer.producer.send(message);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        simpleProducer.producer.close();
    }
}