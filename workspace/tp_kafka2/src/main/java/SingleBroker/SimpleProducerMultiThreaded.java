package SingleBroker;

import org.apache.kafka.clients.producer.ProducerRecord;

public class SimpleProducerMultiThreaded extends Thread {
    private String threadedTopic;
    private Thread thread;
    private int sleepTime;

    public SimpleProducerMultiThreaded(String threadName, int sleepTime) {
        this.threadedTopic = threadName;
        this.sleepTime = sleepTime;
        System.out.println("Creation de la thread '" + this.threadedTopic + "'");
    }

    public void run() {
        SimpleProducer simpleProducer = new SimpleProducer();

        for (int i = 0; i < 100; i++) {
            String messageStr = "Message #" + i + " pour le topic '" + this.threadedTopic + "'";
            ProducerRecord<String, String> message = new ProducerRecord<String, String>(
                    this.threadedTopic,
                    Integer.toString(i), messageStr);
            simpleProducer.producer.send(message);
            try {
                Thread.sleep(this.sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void start() {
        System.out.println("Demarrage de la thread '" + this.threadedTopic + "'");
        if (this.thread == null) {
            this.thread = new Thread(this, this.threadedTopic);
            this.thread.start();
        }
    }

    public static void main(String[] args) {
        SimpleProducerMultiThreaded producer1 = new
                SimpleProducerMultiThreaded("topic_1", 1000);
        producer1.start();

        SimpleProducerMultiThreaded producer2 =
                new SimpleProducerMultiThreaded("topic_2", 2000);
        producer2.start();

        SimpleProducerMultiThreaded producer3 =
                new SimpleProducerMultiThreaded("topic_3", 3000);
        producer3.start();
    }
}
