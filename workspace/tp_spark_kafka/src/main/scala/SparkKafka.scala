import org.apache.spark.{SparkConf, SparkContext}
import kafka.serializer.StringDecoder
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka.KafkaUtils


object SparkKafka {
  def main(args: Array[String]): Unit = {
    // Création du contexte Spark Streaming
    val sparkConf = new SparkConf()
      .setMaster("local[*]")
      .setAppName("WordcountSparkScala")

    val parentSlideDuration: Int = 2; // secondes
    val ssc = new StreamingContext(sparkConf, Seconds(parentSlideDuration))
    ssc.sparkContext.setLogLevel("ERROR")

    // AdresseIP:port pour les brokers Kafka
    val kafkaParams = Map[String, String](
      "metadata.broker.list" -> "localhost:9092")

    // Liste des topics pour lesquels souscrire
    val topics = List("kafka-streams").toSet

    // Création du stream Kafka qui contiendra les paires (topic, message)
    // et récupération des messages.
    val lines = KafkaUtils.createDirectStream[String, String,
      StringDecoder, StringDecoder](ssc, kafkaParams, topics)
      .map(_._2)

    // Tokenisation des lignes
    val words = lines.flatMap(_.split(" "))

    // Comptage des mots sur une fenêtre de 60 secondes
    val wordsCount = words.map(x => (x, 1))
      .reduceByKeyAndWindow(_ + _, Seconds(60))
    wordsCount.print()

    ssc.checkpoint("checkpoint") // dossier de sauvegarde des checkpoints
                                // en cas de 'plantage' du programme
    ssc.start() // Début du traitement des flux
    ssc.awaitTermination()
    ssc.stop()
  }
}
