name := "tp_spark_kafka"

version := "0.1"

scalaVersion := "2.11.8"

//resolvers += "Spark Packages Repo" at "https://dl.bintray.com/spark-packages/maven"

scalacOptions ++= Seq("-deprecation")

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.6.2",
  "org.apache.spark" %% "spark-sql" % "1.6.2",
  "org.apache.spark" %% "spark-streaming" % "1.6.2",
  "org.apache.spark" %% "spark-streaming-kafka" % "1.6.2",
  "org.apache.kafka" %% "kafka" % "0.8.2.1",
  "junit" % "junit" % "4.10" % "test"
)


