# MOD10-TP Apache Kafka

### Description

Travaux pratiques de la formation **Apache Kafka (Applications avec Java et Scala)** :

### Auteur

2017-, Fabrice LEBEL, fabrice.lebel.pro@outlook.com, [LinkedIn](https://www.linkedin.com/in/fabrice-lebel-b850674?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BbqGekl60S9W3uZDa6jyWkg%3D%3D)

### Repo

https://bitbucket.org/fableb/mod10_tp-apache_kafka/src/master
